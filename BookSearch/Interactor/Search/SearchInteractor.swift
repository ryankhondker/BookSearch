//
//  SearchInteractor.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

class SearchInteractor {
    weak var presenter: SearchPresenter?
    
    func getResultsFromApi(queryString: String) {
        ApiService.getResults(queryString: queryString) {
            (bookResults, error) in
            if let err = error {
                self.presenter?.showError(error: err)
                return
            }
            guard let bookResultsReturned = bookResults else {
                return
            }
            self.saveResultsToDb(results: bookResultsReturned)
            self.presenter?.showResultsView()
            return
        }
    }
    
    private func saveResultsToDb(results: BookResults) {
        deleteExistingRecordsInDb()
        results.items.forEach {
            (bookResultItem) in
            let newRealmBook = RealmBook()
            newRealmBook.setup(json: bookResultItem.book)
            RealmManager.addBookToDB(book: newRealmBook) {
                [unowned self] (addWasSuccessful) in
                if !addWasSuccessful {
                    self.presenter?.showError(error: RealmErrors.addUnsuccessful)
                }
            }
        }
    }
    
    private func deleteExistingRecordsInDb() {
        RealmManager.deleteAllFromDatabase {
            [unowned self] (deleteWasSuccessful) in
            if !deleteWasSuccessful {
                self.presenter?.showError(error: RealmErrors.deleteUnsuccessful)
            }
        }
    }
}
