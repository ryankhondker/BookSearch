//
//  ResultsInteractor.swift
//  BookSearch
//
//  Created by Ryan Khondker on 5/6/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

class ResultsInteractor {
    weak var presenter: ResultsPresenter?
    let books: [Book]
    
    init() {
        //bookResults = Array(RealmManager.getDataFromDB())
        books = RealmManager.getDataFromDB()
    }
}
