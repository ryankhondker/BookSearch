//
//  SearchViewToSearchPresenter.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

protocol SearchViewToSearchPresenter {
    func callApi(query: String)
}
