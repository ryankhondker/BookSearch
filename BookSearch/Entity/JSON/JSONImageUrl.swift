//
//  JSONImageUrl.swift
//  BookSearch
//
//  Created by Ryan Khondker on 5/8/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

struct JSONImageUrl : Codable {
    let thumbnailUrl: String?
    
    enum CodingKeys : String, CodingKey {
        case thumbnailUrl = "thumbnail"
    }
}
