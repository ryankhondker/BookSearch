//
//  BookResults.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

struct BookResults : Codable {
    let items: [BookResultItem]
    
    enum CodingKeys : String, CodingKey {
        case items
    }
}
