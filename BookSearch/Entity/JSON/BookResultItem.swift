//
//  BookResultItem.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

struct BookResultItem : Codable {
    let book: JSONBook
    
    enum CodingKeys : String, CodingKey {
        case book = "volumeInfo"
    }
}
