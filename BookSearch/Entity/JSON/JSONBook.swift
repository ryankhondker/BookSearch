//
//  JSONBook.swift
//  BookSearch
//
//  Created by Ryan Khondker on 5/7/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

struct JSONBook: Codable {
    var title: String?
    var authors: [String]?
    var imageUrl: JSONImageUrl?
    
    enum CodingKeys : String, CodingKey {
        case title
        case authors
        case imageUrl = "imageLinks"
    }
}
