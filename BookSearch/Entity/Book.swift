//
//  Book.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation
import RealmSwift

struct Book {
    let uuid: String?
    let title: String?
    let authors: [String]?
    let thumbnailUrl: String?
    
    init(json: JSONBook) {
        uuid = UUID().uuidString
        self.title = json.title
        //self.imageUrl = ImageUrl(thumbnailUrl: json.imageUrl?.thumbnailUrl)
        self.thumbnailUrl = json.imageUrl?.thumbnailUrl
        self.authors = json.authors
    }
    
    init(realm: RealmBook) {
        uuid = realm.bookID
        self.title = realm.title
        //self.imageUrl = ImageUrl(thumbnailUrl: realm.imageUrl?.thumbnailUrl)
        self.thumbnailUrl = realm.thumbnailUrl
        self.authors = Array(realm.authors)
    }
}
