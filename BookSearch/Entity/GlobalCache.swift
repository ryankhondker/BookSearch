//
//  GlobalCache.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class GlobalCache {
    static let shared = GlobalCache()
    var imageCache = NSCache<NSString, UIImage>()
}
