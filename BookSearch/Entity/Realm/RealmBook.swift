//
//  RealmBook.swift
//  BookSearch
//
//  Created by Ryan Khondker on 5/7/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation
import RealmSwift

class RealmBook : Object {
    @objc dynamic var bookID = UUID().uuidString
    @objc dynamic var title: String?
    @objc dynamic var thumbnailUrl: String?
    
    var authors = List<String>()
    //var authors = List<Author>()
    
    func setup(json: JSONBook) {
        title = json.title
        thumbnailUrl = json.imageUrl?.thumbnailUrl
        if let jsonAuthors = json.authors {
            jsonAuthors.forEach {
                authorName in
                /*
                let author = Author()
                author.setup(name: authorName)
                authors.append(author)*/
                authors.append(authorName)
            }
        }
    }
}
