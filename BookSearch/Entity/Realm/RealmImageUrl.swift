//
//  RealmImageUrl.swift
//  BookSearch
//
//  Created by Ryan Khondker on 5/8/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation
import RealmSwift

class RealmImageUrl : Object {
    @objc dynamic var thumbnailUrl: String?
    
    convenience init(imageUrl: JSONImageUrl?) {
        self.init()
        self.thumbnailUrl = imageUrl?.thumbnailUrl
    }
}
