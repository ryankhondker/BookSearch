//
//  Author.swift
//  BookSearch
//
//  Created by Ryan Khondker on 5/7/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation
import RealmSwift

class Author : Object {
    @objc dynamic var name: String = ""
    
    //let books = LinkingObjects(fromType: RealmBook.self, property: "authors")
    
    func setup(name: String) {
        self.name = name
    }
}
