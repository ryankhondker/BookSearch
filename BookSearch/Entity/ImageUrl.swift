//
//  ImageUrl.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation
import RealmSwift

struct ImageUrl {
    let thumbnailUrl: String?
}
