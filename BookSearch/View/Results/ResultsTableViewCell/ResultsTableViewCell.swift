//
//  ResultsTableViewCell.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ResultsTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var bookImage: UIImageView!
    
    lazy var session = URLSession(configuration: .default)

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(book: Book) {
        if let title = book.title {
            titleLabel.text = title
        }
        if let authors = book.authors {
            authorLabel.text = authors.joined(separator: ", ")
        }
        session.invalidateAndCancel()
        session = URLSession(configuration: .default)
        //guard let imageUrl = book.imageUrl else { return }
        guard let thumbnailUrl = book.thumbnailUrl else { return }
        ImageService.getImage(imageUrl: thumbnailUrl) {
            [unowned self] (bookImage, error) in
            DispatchQueue.main.async {
                self.bookImage.image = bookImage
            }
        }
    }
}
