//
//  ResultsViewController.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    @IBOutlet weak var resultsTableView: UITableView!
    
    var presenter = ResultsPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.view = self
        registerTableViewToViewController()
        registerTableViewCell()
    }
    
    private func registerTableViewToViewController() {
        resultsTableView.dataSource = self
        resultsTableView.delegate = self
    }
    
    private func registerTableViewCell() {
        let bundle = Bundle(for: ResultsTableViewCell.self)
        let nib = UINib(nibName: "ResultsTableViewCell", bundle: bundle)
        resultsTableView.register(nib, forCellReuseIdentifier: "ResultsCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ResultsViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getResultsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let resultsCell = tableView.dequeueReusableCell(withIdentifier: "ResultsCell") as? ResultsTableViewCell else {
            fatalError("ResultsCell does not exist")
        }
        let book = presenter.getBookAtIndex(index: indexPath.row)
        resultsCell.setCell(book: book)
        return resultsCell
    }
}

extension ResultsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let resultsCell = cell as? ResultsTableViewCell else { return }
        resultsCell.session.invalidateAndCancel()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
