//
//  SearchViewController.swift
//  BookSearch
//
//  Created by Mac on 4/29/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    @IBOutlet weak var searchTextField: UITextField!
    
    let presenter = SearchPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        presenter.view = self
        searchTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showErrorAlert(error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okButton)
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    func showResultsView(resultsVC: ResultsViewController) {
        DispatchQueue.main.async {
            self.show(resultsVC, sender: nil)
        }
    }

    @IBAction func searchPressed() {
        searchTextField.resignFirstResponder()
        guard let searchQuery = searchTextField.text else { return }
        presenter.callApi(query: searchQuery)
    }
}

extension SearchViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchPressed()
        return true
    }
}
