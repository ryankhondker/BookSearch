//
//  SearchPresenter.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

class SearchPresenter {
    let interactor = SearchInteractor()
    let router = SearchRouter()
    weak var view: SearchViewController?
    
    init() {
        interactor.presenter = self
    }
    
    func callApi(query: String) {
        //let queryString = "\(query.replacingOccurrences(of: " ", with: "+"))"
        guard let queryString = query.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            view?.showErrorAlert(error: NetworkErrors.invalidQuery)
            return
        }
        interactor.getResultsFromApi(queryString: queryString)
    }
    
    func showResultsView() {
        view?.showResultsView(resultsVC: router.getResultsView())
    }
    
    func showError(error: Error) {
        view?.showErrorAlert(error: error)
    }
}
