//
//  ResultsPresenter.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

class ResultsPresenter {
    let interactor = ResultsInteractor()
    weak var view: ResultsViewController?
    
    init() {
        interactor.presenter = self
    }
    
    func getResultsCount() -> Int {
        return interactor.books.count
    }
    
    func getBookAtIndex(index: Int) -> Book {
        return interactor.books[index]
    }
}
