//
//  RealmErrors.swift
//  BookSearch
//
//  Created by Mac on 5/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

enum RealmErrors : Error {
    case addUnsuccessful
    case deleteUnsuccessful
}
