//
//  NetworkErrors.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

enum NetworkErrors : Error {
    case invalidQuery
    case badUrl
    case badResponse
    case httpError(code: Int)
    case noData
    case badData
}
