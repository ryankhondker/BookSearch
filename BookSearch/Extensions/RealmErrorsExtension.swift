//
//  RealmErrorsExtension.swift
//  BookSearch
//
//  Created by Mac on 5/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

extension RealmErrors : LocalizedError {
    private var errorDescription: String {
        switch self {
        case .addUnsuccessful:
            return "Error saving to database"
        case .deleteUnsuccessful:
            return "Error deleting from database"
        }
    }
}
