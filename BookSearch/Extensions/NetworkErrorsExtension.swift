//
//  NetworkErrorsExtension.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

extension NetworkErrors : LocalizedError {
    private var errorDescription: String {
        switch self {
        case .invalidQuery:
            return "Unable to encode query to URL"
        case .badUrl:
            return "Invalid URL"
        case .badResponse:
            return "Bad response from server"
        case let .httpError(code: errorCode):
            return "\(errorCode) Error received from server"
        case .noData:
            return "No data received from server"
        case .badData:
            return "Bad data received from server"
        }
    }
}
