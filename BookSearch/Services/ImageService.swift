//
//  ImageService.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ImageService {
    class func getImage(imageUrl: String, completion: @escaping (UIImage?, Error?) -> ()) {
        if let image = GlobalCache.shared.imageCache.object(forKey: imageUrl as NSString) {
            completion(image, nil)
            return
        }
        guard let url = URL(string: imageUrl) else {
            completion(nil, NetworkErrors.badUrl)
            return
        }
        let session = URLSession.shared
        let task = session.dataTask(with: url) {
            (data, response, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, NetworkErrors.badResponse)
                return
            }
            
            guard httpResponse.statusCode == 200 else {
                completion(nil, NetworkErrors.httpError(code: httpResponse.statusCode))
                return
            }
            
            guard let data = data else {
                completion(nil, NetworkErrors.noData)
                return
            }
            guard let image = UIImage(data: data) else {
                completion(nil, NetworkErrors.badData)
                return
            }
            GlobalCache.shared.imageCache.setObject(image, forKey: imageUrl as NSString)
            completion(image, nil)
            return
        }
        task.resume()
    }
}
