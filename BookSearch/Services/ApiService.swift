//
//  ApiService.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation

class ApiService {
    static let kBaseUrl = "https://www.googleapis.com/books/v1/volumes?q="
    
    class func getResults(queryString: String, completion: @escaping (BookResults?, Error?) -> ()) {
        let urlAsString = "\(kBaseUrl)\(queryString)"
        guard let url = URL(string: urlAsString) else {
            completion(nil, NetworkErrors.badUrl)
            return
        }
        let session = URLSession.shared
        let task = session.dataTask(with: url) {
            (data, response, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                completion(nil, NetworkErrors.badResponse)
                return
            }
            
            guard response.statusCode == 200 else {
                completion(nil, NetworkErrors.httpError(code: response.statusCode))
                return
            }
            
            guard let data = data else {
                completion(nil, NetworkErrors.noData)
                return
            }
            
            do {
                let bookResults = try JSONDecoder().decode(BookResults.self, from: data)
                completion(bookResults, nil)
                return
            }
            catch let error {
                completion(nil, error)
                return
            }
        }
        task.resume()
    }
}
