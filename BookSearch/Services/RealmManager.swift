//
//  RealmManager.swift
//  BookSearch
//
//  Created by Mac on 5/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    static let backgroundQueue = DispatchQueue(label: "background")
    //static let shared = RealmManager()
    
    class func getDataFromDB() -> [Book] {
        var books: [Book] = []
        backgroundQueue.sync {
            if let database = try? Realm() {
                let results = Array(database.objects(RealmBook.self))
                results.forEach {
                    realmBook in
                    let book = Book(realm: realmBook)
                    books.append(book)
                }
            }
        }
        return books
    }
    
    class func addBookToDB(book: RealmBook, completion: @escaping (Bool) -> ()) {
        backgroundQueue.async {
            if let database = try? Realm() {
                do {
                    try database.write {
                        database.add(book)
                        completion(true)
                    }
                }
                catch {
                    completion(false)
                }
            }
        }
    }
    
    class func deleteAllFromDatabase(completion: @escaping (Bool) -> ()) {
        backgroundQueue.async {
            if let database = try? Realm() {
                do {
                    try database.write {
                        database.deleteAll()
                        completion(true)
                    }
                }
                catch {
                    completion(false)
                }
            }
        }
    }
}
