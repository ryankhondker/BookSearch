//
//  SearchRouter.swift
//  BookSearch
//
//  Created by Mac on 4/30/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class SearchRouter {
    func getResultsView() -> ResultsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let resultsVC = storyboard.instantiateViewController(withIdentifier: "ResultsVC") as? ResultsViewController else {
            fatalError("ResultsVC does not exist")
        }
        return resultsVC
    }
}
